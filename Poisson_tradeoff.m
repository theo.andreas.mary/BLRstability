clear all; close all;

lrthlist=10.^(-15:-4);
fprintf('matrix = P64\n');
ind=0;
for lrth=lrthlist
  ind = ind+1;
  fprintf('lrth = %.0e\n',lrth);
  [berr64(ind,1),flops64(ind,1)]=BLRstability('P64',256,lrth*1e6,2,'d','UFC',0,'tmp');
  [berr64(ind,2),flops64(ind,2)]=BLRstability('P64',256,lrth,4,'d','UCF',1,'tmp');
end
fprintf('matrix = P128\n');
ind=0;
for lrth=lrthlist
  ind = ind+1;
  fprintf('lrth = %.0e\n',lrth);
  [berr128(ind,1),flops128(ind,1)]=BLRstability('P128',256,lrth*1e6,2,'d','UFC',0,'tmp');
  [berr128(ind,2),flops128(ind,2)]=BLRstability('P128',256,lrth,4,'d','UCF',1,'tmp');
end

cutofflist = 10.^(-13:2:-5);
indC = 0;
for cutoff = cutofflist
  indC = indC+1;
  indP = 0;
  for Psiz = [64 128]
    switch(Psiz)
      case  64, berr=berr64;  flops=flops64;
      case 128, berr=berr128; flops=flops128;
    end
    indP = indP+1;
    F = (berr<=cutoff);
    for j=1:2
      i=find(F(:,j),1,'last');
      if isempty(i)
        fprintf(' For j=%d, no lrth meets the required cutoff %.1e.\n Taking the smallest for which berr = %.1e\n',j,cutoff,berr(end,j));
        minflop(indP,indC,j)=flops(end,j);
      else
        minflop(indP,indC,j)=flops(i,j);
      end
    end
  end
end

indP = 0;
for Psiz = [64 128]
  switch(Psiz)
    case  64, berr=berr64;  flops=flops64;
    case 128, berr=berr128; flops=flops128;
  end
  indP = indP+1;

  figure()
  semilogy(flops(1:end-2,1),berr(1:end-2,1),'-o','linewidth',lw,'markersize',ms);
  hold on;
  semilogy(flops(:,2),berr(:,2),'-v','linewidth',lw,'markersize',ms);
  indC = 0;
  for cutoff = cutofflist
    indC = indC+1;
    line([min(min(flops)) max(max(flops))],[cutoff cutoff],'linestyle','--','color','k','linewidth',1);
    htxt = text(mean(minflop(indP,indC,:)), cutoff*2.5, sprintf('%.1f',minflop(indP,indC,1)./minflop(indP,indC,2)));
    set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  end

  set(gca,'fontsize',fs);
  hleg=legend('Local, UFC, without recompressions','Global, UCF, with recompressions');
  set(hleg,'interpreter','latex','fontsize',fs,'location','northoutside','numcolumns',1,'orientation','horizontal');

  xlabel('Flops','interpreter','latex','fontsize',fs);
  ylabel('Backward error','interpreter','latex','fontsize',fs);
  xlim([-inf inf]);
  ylim([-inf inf]);
  yticks(10.^(-14:2:-4))
end

