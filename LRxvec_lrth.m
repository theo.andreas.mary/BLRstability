clear all; close all;

n = 1024; m = 1;
A = gallery('randsvd',n,1e16,3);
V = rand(n,m);
nrmA = mynorm(A);
nrmV = mynorm(V);
[UL,S,UR] = svd(A);
Svec = diag(S);

ind=0;
lrthlist=10.^(-15:0);
for lrth=lrthlist

  ind = ind+1;
  rk = 1;
  while rk<n && sqrt(sum(Svec(rk+1:end).^2))>lrth*nrmA
    rk = rk+1;
  end
  X = UL(:,1:rk);
  Y = S(1:rk,1:rk)*UR(:,1:rk)';
  
  Z0 = A*V;
  Z1 = X*(Y*V);
  Z2 = double(single(X)*(single(Y)*single(V)));
  Z3 = matmul(X,matmul(Y,V,11),11);
  berr1(ind) = mynorm(Z0-Z1)/(nrmA*nrmV);
  berr2(ind) = mynorm(Z0-Z2)/(nrmA*nrmV);
  berr3(ind) = mynorm(Z0-Z3)/(nrmA*nrmV);
end

loglog(lrthlist,berr3,'-o','linewidth',lw,'markersize',ms);
hold on;
loglog(lrthlist,berr2,'-v','linewidth',lw,'markersize',ms);
loglog(lrthlist,berr1,'-s','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Half precision','Single precision','Double precision');
set(hleg,'interpreter','latex','fontsize',fs,'location','southeast');
xlabel('$$\varepsilon$$','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);

function nrm = mynorm(A)
  nrm = norm(A,'fro');
end

function C = matmul(A,B,t)
  [m,n] = size(A);
  p = size(B,2);
  C = zeros(m,p);
  for k = 1:n
    C = chop(C + chop(A(:,k)*B(k,:),t),t);
  end
end
