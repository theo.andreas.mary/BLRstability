# BLRstability

MATLAB codes to perform the numerical experiments reported in the article
"Solving Block Low-Rank Linear Systems by LU Factorization is Numerically
Stable" by N. J.  Higham and T. Mary.

## Included MATLAB files
* **BLRstability.m**: computes the BLR LU factorization of a dense matrix.
* **read_matrix.m**: reads a BLR matrix provided in the folder MATRICES/
* **LRxvec_lrth.m**: generates Figure 3.1 of the paper.
* **LRxmat_strassen.m**: generates Figure 3.2 of the paper.
* **BLRxvec_growth.m**: generates Figure 4.1(a) of the paper.
* **BLRxvec_tradeoff.m**: generates Figure 4.1(b) of the paper.
* **UFC_betatype.m**: generates Figure 4.2 of the paper.
* **UFC_lrth.m**: generates Figure 4.3(a) of the paper.
* **UFC_growth.m**: generates Figure 4.3(b) of the paper.
* **recomp_growth.m**: generates Figure 4.4(a) of the paper.
* **recomp_tradeoff.m**: generates Figure 4.4(b) of the paper.
* **UCF_growth.m**: generates Figure 4.5(a) of the paper.
* **UCF_tradeoff.m**: generates Figure 4.5(b) of the paper.
* **SuiteSparse_lrth.m**: generates Figure 5.1 of the paper.
* **Poisson_tradeoff.m**: generates Figure 5.2 of the paper.
* **SuiteSparse_tradeoff.m**: generates Figure 5.3 of the paper.

## Included Matrices
The Poisson matrices are included in the folder MATRICES/ and can be read using the script **read_matrix.m**. 
The SuiteSparse matrices are NOT included, as these take significant space. To
get access to these please contact the authors.

## Requirements
* The codes have been developed and tested with MATLAB 2018b.
* The scripts **LRxvec_lrth.m** and **UFC_lrth.m** require the function **chop.m**
from the [Matrix Computations Toolbox](http://www.ma.man.ac.uk/~higham/mctoolbox/).
