clear all; close all;

lrthlist=10.^(-12:-4);

for betatype=1:4
  ind=0;
  for lrth=lrthlist
    ind = ind+1;
    fprintf('lrth = %.0e (type %d)\n',lrth,betatype);
    [berr(ind,betatype),flops(ind,betatype),Lsto(ind,betatype),Usto(ind,betatype)]=BLRstability('P64',256,lrth,betatype,'d','UFC',0,'tmp');
  end
end

semilogy(flops(:,3),berr(:,3),'-o','linewidth',lw,'markersize',ms);
hold on;
semilogy(flops(:,1),berr(:,1),'-v','linewidth',lw,'markersize',ms);
semilogy(flops(:,4),berr(:,4),'-s','linewidth',lw,'markersize',ms);
semilogy(flops(:,2),berr(:,2),'-*','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Global','Local','Global scaled','Local scaled');
set(hleg,'interpreter','latex','fontsize',fs,'location','northeast');
xlabel('Flops','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf inf]);

figure()
semilogx(lrthlist,Lsto(:,3)./Usto(:,3),'-o','linewidth',lw,'markersize',ms);
hold on; 
semilogx(lrthlist,Lsto(:,1)./Usto(:,1),'-v','linewidth',lw,'markersize',ms);
semilogx(lrthlist,Lsto(:,4)./Usto(:,4),'-s','linewidth',lw,'markersize',ms);
semilogx(lrthlist,Lsto(:,2)./Usto(:,2),'-*','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Global','Local','Global scaled','Local scaled');
set(hleg,'interpreter','latex','fontsize',fs,'location','northwest');
xlabel('$$\varepsilon$$','interpreter','latex','fontsize',fs);
ylabel('L/U','interpreter','latex','fontsize',fs,'rotation',0);
xlim([-inf inf]);
ylim([-inf inf]);

