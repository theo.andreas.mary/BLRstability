clear all; close all;

lrthlist=10.^(-14:-2);

ind=0;
for lrth=lrthlist
  ind = ind+1;
  fprintf('lrth = %.0e\n',lrth);
  [berr(ind,1),flops(ind,1)]=BLRstability('P64',256,lrth,4,'d','UFC',0,'tmp');
  [berr(ind,2),flops(ind,2)]=BLRstability('P64',256,lrth,4,'d','UFC',1,'tmp');
end

semilogy(flops(:,1),berr(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;
semilogy(flops(:,2),berr(:,2),'-v','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Without recompression', 'With recompression');
set(hleg,'interpreter','latex','fontsize',fs,'location','northeast');

xlabel('Flops','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf inf]);

