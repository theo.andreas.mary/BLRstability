clear all; close all;

matrixlist = {'2cubes_sphere','af_shell3','audikw_1','cfd2','Dubcova3','hood','oilpan','pwtk','shallow_water1','ship_003','thermomech_dM','x104',...
'atmosmodd','cage13','ML_Geer','Transport','Flan_1565','Serena','Hook_1498','nlpkkt80','Long_Coup_dt0','Cube_Coup_dt0','Geo_1438','nd24k','Emilia_923',...
'Fault_639'};

lrthlist = [1e-7 5e-8 2e-8 1e-8 5e-9 2e-9 1e-9];

indmat=0;
for matrix=matrixlist
  indmat = indmat+1;
  ind=0;
  fprintf('matrix = %s\n',matrix{1});
  for lrth=lrthlist
    ind = ind+1;
    fprintf(' lrth = %.0e\n',lrth);
    [berr(ind,1,indmat),flops(ind,1,indmat)]=BLRstability(matrix{1},256,lrth*100,2,'d','UFC',0,'tmp');
    [berr(ind,2,indmat),flops(ind,2,indmat)]=BLRstability(matrix{1},256,lrth*10,2,'d','UCF',0,'tmp');
    [berr(ind,3,indmat),flops(ind,3,indmat)]=BLRstability(matrix{1},256,lrth*10,2,'d','UFC',1,'tmp');
    [berr(ind,4,indmat),flops(ind,4,indmat)]=BLRstability(matrix{1},256,lrth*10,2,'d','UCF',1,'tmp');
    [berr(ind,5,indmat),flops(ind,5,indmat)]=BLRstability(matrix{1},256,lrth*10,4,'d','UFC',0,'tmp');
    [berr(ind,6,indmat),flops(ind,6,indmat)]=BLRstability(matrix{1},256,lrth,4,'d','UCF',0,'tmp');
    [berr(ind,7,indmat),flops(ind,7,indmat)]=BLRstability(matrix{1},256,lrth,4,'d','UFC',1,'tmp');
    [berr(ind,8,indmat),flops(ind,8,indmat)]=BLRstability(matrix{1},256,lrth,4,'d','UCF',1,'tmp');
  end
end
% sort by increasing n
I=[11 9 5 7 6 10 8 2 4 12 17 15 1 3 16 19 24 26 13 25 23 20 21 18 22 14];
berr   = berr(:,:,I);
flops  = flops(:,:,I);
  
indmat=0;
for matrix=matrixlist
  indmat = indmat+1;
  % select cutoff point as avg error in log scale
  cutoff(indmat) = 10^mean(mean(log10(berr(:,:,indmat))));
  F = (berr(:,:,indmat)<=cutoff(indmat));
  for j=1:8
    i=find(F(:,j),1);
    if isempty(i)
      fprintf(' For indmat=%d, j=%d, no lrth meets the required cutoff %.1e.\n Taking the smallest for which berr = %.1e\n',...
      indmat,j,cutoff(indmat),berr(end,j,indmat));
      minflop(indmat,j)=flops(end,j,indmat);
    else
      minflop(indmat,j)=flops(i,j,indmat);
    end
  end
end

% performance profile
best = min(minflop,[],2);
xlist= linspace(1,10,200); i=0;
for x=xlist
  i=i+1;
  perf_data(i,:) = sum(minflop./best<=x)./size(minflop,1)*100;
end
markfreq=10;  
h1=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,8),'p','linewidth',lw,'markersize',ms);
hold on;
h2=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,6),'v','linewidth',lw,'markersize',ms);
h3=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,7),'s','linewidth',lw,'markersize',ms);
h4=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,5),'*','linewidth',lw,'markersize',ms);
h5=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,4),'d','linewidth',lw,'markersize',ms);
h6=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,2),'x','linewidth',lw,'markersize',ms);
h7=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,3),'^','linewidth',lw,'markersize',ms);
h8=plot(xlist(1:markfreq:end),perf_data(1:markfreq:end,1),'o','linewidth',lw,'markersize',ms);
plot(xlist,perf_data(:,8),'-','linewidth',lw,'markersize',ms,'color',get(h1,'color'));
plot(xlist,perf_data(:,6),'-','linewidth',lw,'markersize',ms,'color',get(h2,'color'));
plot(xlist,perf_data(:,7),'-','linewidth',lw,'markersize',ms,'color',get(h3,'color'));
plot(xlist,perf_data(:,5),'-','linewidth',lw,'markersize',ms,'color',get(h4,'color'));
plot(xlist,perf_data(:,4),'-','linewidth',lw,'markersize',ms,'color',get(h5,'color'));
plot(xlist,perf_data(:,2),'-','linewidth',lw,'markersize',ms,'color',get(h6,'color'));
plot(xlist,perf_data(:,3),'-','linewidth',lw,'markersize',ms,'color',get(h7,'color'));
plot(xlist,perf_data(:,1),'-','linewidth',lw,'markersize',ms,'color',get(h8,'color'));
%hleg=legend('Local, UFC, w/o rec.', 'Local, UCF, w/o rec.','Local, UFC, w/ rec.', 'Local, UCF, w/ rec.',...
%            'Global, UFC, w/o rec.', 'Global, UCF, w/o rec.','Global, UFC, w/ rec.', 'Global, UCF, w/ rec.');
hleg=legend('Global, UCF, w/ rec.', 'Global, UCF, w/o rec.','Global, UFC, w/ rec.', 'Global, UFC, w/o rec.',...
            'Local,  UCF, w/ rec.', 'Local,  UCF, w/o rec.','Local,  UFC, w/ rec.', 'Local,  UFC, w/o rec.');
set(hleg,'interpreter','latex','fontsize',fs,'location','eastoutside','numcolumns',1);
set(gca,'fontsize',fs);
xlim([1 10]);
ylim([0 100]);
xlabel('$$\alpha$$','interpreter','latex','fontsize',fs);
xticks(1:10)
ylabel('$$\rho$$','interpreter','latex','fontsize',fs,'rotation',0);
pos=get(gcf,'position'); pos(3)=1000;
set(gcf,'position',pos);

