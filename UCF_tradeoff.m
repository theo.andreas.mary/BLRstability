clear all; close all;

lrthlist=10.^(-14:-2);

ind=0;
for lrth=lrthlist
  ind = ind+1;
  fprintf('lrth = %.0e\n',lrth);
  [berr(ind,1),flops(ind,1)]=BLRstability('P64',256,lrth,4,'d','UFC',0,'tmp');
  [berr(ind,2),flops(ind,2)]=BLRstability('P64',256,lrth,4,'d','UCF',0,'tmp');
  [berr(ind,3),flops(ind,3)]=BLRstability('P64',256,lrth,4,'d','UFC',1,'tmp');
  [berr(ind,4),flops(ind,4)]=BLRstability('P64',256,lrth,4,'d','UCF',1,'tmp');
end

h1=semilogy(flops(:,1),berr(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;
h2=semilogy(flops(:,2),berr(:,2),'-v','linewidth',lw,'markersize',ms);
semilogy(flops(:,3),berr(:,3),'--o','linewidth',lw,'markersize',ms,'color',get(h1,'color'));
semilogy(flops(:,4),berr(:,4),'--v','linewidth',lw,'markersize',ms,'color',get(h2,'color'));
set(gca,'fontsize',fs);
hleg=legend('UFC','UCF','UFC+recompression','UCF+recompression');
set(hleg,'interpreter','latex','fontsize',fs,'location','northeast');

xlabel('Flops','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf inf]);

