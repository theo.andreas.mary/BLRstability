clear all; close all;

n = 1024; m = 128;
A = gallery('randsvd',n,1e16,3);
V = rand(n,m);
nrmA = mynorm(A);
nrmV = mynorm(V);
[UL,S,UR] = svd(A);
Svec = diag(S);
Z0 = A*V;
As = single(A); Vs = single(V);
Z1 = double(As*Vs);
Z2 = double(matmul_strassen(As,Vs));

ind=0;
lrthlist=10.^(-7:1:-1);
for lrth=lrthlist

  ind = ind+1;
  rk = 1;
  while rk<n && sqrt(sum(Svec(rk+1:end).^2))>lrth*nrmA
    rk = rk+1;
  end
  rk
  X = UL(:,1:rk);
  Y = S(1:rk,1:rk)*UR(:,1:rk)';
  Xs = single(X); Ys = single(Y);

  Z3 = double(Xs*(Ys*Vs));
  Z4 = double(matmul_strassen(Xs,matmul_strassen(Ys,Vs)));
  err1(ind) = mynorm(Z0-Z1)/(nrmA*nrmV);
  err2(ind) = mynorm(Z0-Z2)/(nrmA*nrmV);
  err3(ind) = mynorm(Z0-Z3)/(nrmA*nrmV);
  err4(ind) = mynorm(Z0-Z4)/(nrmA*nrmV);
end

loglog(lrthlist,err1,'-o','linewidth',lw,'markersize',ms);
hold on;
loglog(lrthlist,err2,'-v','linewidth',lw,'markersize',ms);
loglog(lrthlist,err3,'-s','linewidth',lw,'markersize',ms);
loglog(lrthlist,err4,'-*','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Full','Full Strassen','LR','LR Strassen');
set(hleg,'interpreter','latex','fontsize',fs,'location','northwest');
xlabel('$$\varepsilon$$','interpreter','latex','fontsize',fs);
ylabel('Error','interpreter','latex','fontsize',fs);
xlim([1e-7 1e-1]);
ylim([-inf inf]);
xticks(10.^(-7:1:-1));
yticks(10.^(-8:-1));

function nrm = mynorm(A)
  nrm = norm(A,'fro');
end


function C = matmul_strassen(A, B, nmin)
if nargin < 3, nmin = 16; end

  [m,n] = size(A);
  [~,p] = size(B);
  pad = 2^ceil(log2(max(m,max(n,p))));
  A = [A zeros(m,pad-n); zeros(pad-m,pad)];
  B = [B zeros(n,pad-p); zeros(pad-n,pad)];

  if pad <= nmin
    C = A*B;
  else
    h = pad/2; i = 1:h; j = h+1:pad;
    P1 = matmul_strassen( A(i,i)+A(j,j), B(i,i)+B(j,j), nmin);
    P2 = matmul_strassen( A(j,i)+A(j,j), B(i,i), nmin);
    P3 = matmul_strassen( A(i,i), B(i,j)-B(j,j), nmin);
    P4 = matmul_strassen( A(j,j), B(j,i)-B(i,i), nmin);
    P5 = matmul_strassen( A(i,i)+A(i,j), B(j,j), nmin);
    P6 = matmul_strassen( A(j,i)-A(i,i), B(i,i)+B(i,j), nmin);
    P7 = matmul_strassen( A(i,j)-A(j,j), B(j,i)+B(j,j), nmin);
    C = [ P1+P4-P5+P7  P3+P5;  P2+P4  P1+P3-P2+P6 ];
  end

  C = C(1:m,1:p);
end
