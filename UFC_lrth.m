clear all; close all;

lrthD=10.^(-12:1:-1);
lrthS=10.^(-12:1:-1);
lrthH=10.^(-6:1:-1);

ind=0;
for lrth=lrthD
  ind = ind+1;
  fprintf('lrth = %.0e (double)\n',lrth);
  errd(ind)=BLRstability('P64',256,lrth,4,'d','UFC',0,'tmp');
end
ind=0;
for lrth=lrthS
  ind = ind+1;
  fprintf('lrth = %.0e (single)\n',lrth);
  errs(ind)=BLRstability('P64',256,lrth,4,'s','UFC',0,'tmp');
end
ind=0;
for lrth=lrthH
  ind = ind+1;
  fprintf('lrth = %.0e (half)\n',lrth);
  errh(ind)=BLRstability('P64',256,lrth,4,'h','UFC',0,'tmp');
end

loglog(lrthH,errh,'-o','linewidth',lw,'markersize',ms);
hold on;
loglog(lrthS,errs,'-v','linewidth',lw,'markersize',ms);
loglog(lrthD,errd,'-s','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Half precision','Single precision','Double precision');
set(hleg,'interpreter','latex','fontsize',fs,'location','southeast');

xlabel('$$\varepsilon$$','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf inf]);
xticks(10.^(-12:2:-1));
yticks(10.^(-14:2:-4));

