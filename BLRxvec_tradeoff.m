clear all; close all;

[A,n,p] = read_matrix('P64',256);
lrthlist=10.^(-14:-2);
m = 1; 
V = cell(p,1);
for i=1:p
  V{i} = rand(size(A{i,i},2),m);
end
Amat=cell2mat(A);
Vmat=cell2mat(V);
nrmA = mynorm(Amat);
nrmV = mynorm(Vmat);
Z0 = Amat*Vmat;

for lrth_type=1:2
  ind=0;
for lrth=lrthlist
  ind = ind+1;
  fprintf('lrth = %.0e (type %d)\n',lrth,lrth_type);

  for i=1:p
  for j=1:p
    if i~=j
      [X{i,j},Y{i,j}] = compress(A{i,j},lrth,lrth_type,nrmA);
    end
  end
  end
  
  flop = 0;
  Z1 = cell(p,1);
  for i=1:p
    Z1{i} = A{i,i}*V{i};
    flop = flop + 2*prod(size(A{i,i}))*m;
    for j=1:p
      if j~=i
        Z1{i} = Z1{i} + X{i,j}*(Y{i,j}*V{j});
        flop = flop + 2*(size(X{i,j},1)+size(V{j},1))*size(Y{i,j},1)*m;
      end
    end
  end
  Z1 = cell2mat(Z1);
  berr(ind,lrth_type) = mynorm(Z0-Z1)/(nrmA*nrmV);
  flops(ind,lrth_type) = flop;
end
end

h1=semilogy(flops(:,1),berr(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;
h2=semilogy(flops(:,2),berr(:,2),'-^','linewidth',lw,'markersize',ms);
set(gca,'fontsize',fs);
hleg=legend('Local','Global');
set(hleg,'interpreter','latex','fontsize',fs,'location','northeast');

ydata1 = get(h1,'YData');
ydata2 = get(h2,'YData');
len=length(lrthlist);
for i=1:len
  htxt=text(flops(i,1),ydata1(i)*5,sprintf('%d',-log10(lrthlist(i))));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(flops(i,2),ydata2(i)/5,sprintf('%d',-log10(lrthlist(i))));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
end

xlabel('Flops','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf inf]);

function nrm = mynorm(A)
  nrm = norm(A,'fro');
end

function [X,Y] = compress(A,lrth,lrth_type,nrmA)
% compress based on truncated SVD
  [U,S,V] = svd(A);
  if lrth_type==1
    lrth_crit = mynorm(A);
  else
    lrth_crit = nrmA;
  end
  Svec = diag(S);
  rk = 1;
  while rk<length(Svec) && sqrt(sum(Svec(rk+1:end).^2))>lrth*lrth_crit
    rk = rk+1;
  end
  X = U(:,1:rk);
  Y = S(1:rk,1:rk)*V(:,1:rk)';
end
