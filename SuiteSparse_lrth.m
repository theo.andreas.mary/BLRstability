clear all; close all;

matrixlist = {'2cubes_sphere','af_shell3','audikw_1','cfd2','Dubcova3','hood','oilpan','pwtk','shallow_water1','ship_003','thermomech_dM','x104',...
'atmosmodd','cage13','ML_Geer','Transport','Flan_1565','Serena','Hook_1498','nlpkkt80','Long_Coup_dt0','Cube_Coup_dt0','Geo_1438','nd24k','Emilia_923',...
'Fault_639'};

lrthlist = 10.^(-14:-2);
indmat=0;
for matrix=matrixlist
  indmat = indmat+1;
  ind=0;
  fprintf('matrix = %s\n',matrix{1});
  for lrth=lrthlist
    ind = ind+1;
    fprintf(' lrth = %.0e\n',lrth);
    [berr(indmat,ind)]=BLRstability(matrix{1},256,lrth,4,'d','UCF',1,'tmp');
  end
end
% sort by increasing n
I=[11 9 5 7 6 10 8 2 4 12 17 15 1 3 16 19 24 26 13 25 23 20 21 18 22 14];
berr  = berr(I,:)

semilogy(berr(:,13),'-o','linewidth',lw,'markersize',ms);
hold on;
semilogy(berr(:,12),'-v','linewidth',lw,'markersize',ms);
semilogy(berr(:,11),'-s','linewidth',lw,'markersize',ms);
semilogy(berr(:,10),'-<','linewidth',lw,'markersize',ms);
semilogy(berr(:,9),'-x','linewidth',lw,'markersize',ms);
semilogy(berr(:,8),'-d','linewidth',lw,'markersize',ms);
semilogy(berr(:,7),'-*','linewidth',lw,'markersize',ms);
semilogy(berr(:,6),'-^','linewidth',lw,'markersize',ms);
semilogy(berr(:,5),'-+','linewidth',lw,'markersize',ms);
semilogy(berr(:,4),'->','linewidth',lw,'markersize',ms);
semilogy(berr(:,3),'-p','linewidth',lw,'markersize',ms);
semilogy(berr(:,2),'-.','linewidth',lw,'markersize',ms);
semilogy(berr(:,1),'-h','linewidth',lw,'markersize',ms);

for i=1:length(lrthlist)
  legcell{i}=sprintf('$$\\varepsilon=10^{-%d}$$',i+1);
end
set(gca,'fontsize',fs);
legend(legcell,'interpreter','latex','fontsize',fs,'location','eastoutside','numcolumns',1);
xlabel('Matrices (sorted by increasing $$n$$)','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([0.5 size(berr,1)+0.5]);
ylim([1e-17 1e-2]);
xticks([]);
yticks(10.^(-16:2:-2));
pos=get(gcf,'position'); pos(3)=800;
set(gcf,'position',pos);

