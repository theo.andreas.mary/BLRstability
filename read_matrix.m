function [A,n,blknum,Afull] = read_matrix(matrix,blksiz);
  path='MATRICES/';
  %%% read matrix
  f = fopen(sprintf('%s/root_%s_cs%d.bin',path,matrix,blksiz));
  Afull = fread(f,'double');
  fclose(f);
  n = size(Afull,1);
  n = sqrt(n);
  Afull = reshape(Afull,n,n);
  %%% read clusters
  f = fopen(sprintf('%s/clusters_%s_cs%d.txt',path,matrix,blksiz));
  clusters = fscanf(f,'%e');
  fclose(f);
  blknum = length(clusters)-1;
  A = cell(blknum);
  for i=1:blknum
    for j=1:blknum
      A{i,j} = Afull(clusters(i):clusters(i+1)-1,clusters(j):clusters(j+1)-1);
    end
  end
end
