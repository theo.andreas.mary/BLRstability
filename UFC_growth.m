clear all; close all;

ind=0;
for matrix={'P64','P80','P96','P112','P128'}
  ind = ind+1;
  fprintf('matrix = %s\n',matrix{1});
  [berr(ind,1)]=BLRstability(matrix{1},256,1e-04,4,'d','UFC',0,'tmp');
  [berr(ind,2)]=BLRstability(matrix{1},256,1e-08,4,'d','UFC',0,'tmp');
  [berr(ind,3)]=BLRstability(matrix{1},256,1e-12,4,'d','UFC',0,'tmp');
end
nlist=(64:16:128)';
plist=nlist.^2/256;

h1=semilogy(berr(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;
h2=semilogy(berr(:,2),'-v','linewidth',lw,'markersize',ms);
h3=semilogy(berr(:,3),'-s','linewidth',lw,'markersize',ms);

h1b=semilogy(berr(1,1)*plist/plist(1),'--o','linewidth',lw,'markersize',ms,'color',get(h1,'color'));
h2b=semilogy(berr(1,2)*plist/plist(1),'--v','linewidth',lw,'markersize',ms,'color',get(h2,'color'));
h3b=semilogy(berr(1,3)*plist/plist(1),'--s','linewidth',lw,'markersize',ms,'color',get(h3,'color'));

set(gca,'fontsize',fs);
hleg=legend('$$\varepsilon=10^{-4}$$','$$\varepsilon=10^{-8}$$','$$\varepsilon=10^{-12}$$');
set(hleg,'interpreter','latex','fontsize',fs,'numcolumns',3,'location','northoutside');

xlabel('$$n$$','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf inf]);
yticks(10.^(-14:2:-4));
xticklabels(nlist.^2);

