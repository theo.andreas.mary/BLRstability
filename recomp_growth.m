clear all; close all;

nlist = [64:16:128];
for matrix={'P64','P80','P96','P112','P128'}
ind=0;
for blksiz=blist
  ind = ind+1;
  fprintf('matrix = %s\n',matrix{1});
  [errOF(ind,1)]=BLRstability(matrix{1},256,1e-04,4,'d','UFC',0,'tmp');
  [errOF(ind,2)]=BLRstability(matrix{1},256,1e-08,4,'d','UFC',0,'tmp');
  [errOF(ind,3)]=BLRstability(matrix{1},256,1e-12,4,'d','UFC',0,'tmp');
  [errON(ind,1)]=BLRstability(matrix{1},256,1e-04,4,'d','UFC',1,'tmp');
  [errON(ind,2)]=BLRstability(matrix{1},256,1e-08,4,'d','UFC',1,'tmp');
  [errON(ind,3)]=BLRstability(matrix{1},256,1e-12,4,'d','UFC',1,'tmp');
end

h1=semilogy(errOF(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;
h2=semilogy(errOF(:,2),'-v','linewidth',lw,'markersize',ms);
h3=semilogy(errOF(:,3),'-s','linewidth',lw,'markersize',ms);
h1b=semilogy(errON(:,1),'--o','linewidth',lw,'markersize',ms,'color',get(h1,'color'));
h2b=semilogy(errON(:,2),'--v','linewidth',lw,'markersize',ms,'color',get(h2,'color'));
h3b=semilogy(errON(:,3),'--s','linewidth',lw,'markersize',ms,'color',get(h3,'color'));
set(gca,'fontsize',fs);
hleg=legend('Off ($$\varepsilon=10^{-4}$$)','Off ($$\varepsilon=10^{-8}$$)','Off ($$\varepsilon=10^{-12}$$)','On ($$\varepsilon=10^{-4}$$)','On ($$\varepsilon=10^{-8}$$)','On ($$\varepsilon=10^{-12}$$)');
set(hleg,'interpreter','latex','fontsize',fs,'numcolumns',2,'location','northoutside');
xlabel('$$n$$','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
ylim([-inf 1e-3]);
xticklabels(nlist.^2);

ydata1 = get(h1b,'YData');
ydata2 = get(h2b,'YData');
ydata3 = get(h3b,'YData');
for i=1:length(nlist)
  if i==1
    xoffset=0.15;
  elseif i==length(nlist)
    xoffset=-0.15;
  else
    xoffset=0;
  end
  htxt=text(i+xoffset,ydata1(i)*3,sprintf('%.0f',errON(i,1)./errOF(i,1)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(i+xoffset,ydata2(i)*3,sprintf('%.0f',errON(i,2)./errOF(i,2)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(i+xoffset,ydata3(i)*3,sprintf('%.0f',errON(i,3)./errOF(i,3)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
end

