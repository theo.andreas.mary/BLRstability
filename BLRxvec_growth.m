clear all; close all;

m = 1; 
nlist = [32 48 64 80 112 128].^2;
lrthlist = [1e-4 1e-8 1e-12];
matlist={'P32','P48','P64','P80','P112','P128'};
matind=0;
for mat = matlist
  matrix=mat{1}
  matind = matind+1;
  [A,n,p] = read_matrix(matrix,256);
  V = cell(p,1);
  for i=1:p
    V{i} = rand(size(A{i,i},2),m);
  end
  Amat=cell2mat(A);
  Vmat=cell2mat(V);
  nrmA = mynorm(Amat);
  nrmV = mynorm(Vmat);
  Z0 = Amat*Vmat;
  lrthind=0;
  for lrth = lrthlist
    lrthind = lrthind+1;
    for lrth_type=1:2
      X = cell(p); Y = cell(p);
      for i=1:p
      for j=1:p
        if i~=j
          [X{i,j},Y{i,j}] = compress(A{i,j},lrth,lrth_type,nrmA);
        end
      end
      end
      Z1 = cell(p,1);
      for i=1:p
        Z1{i} = A{i,i}*V{i};
        for j=1:p
          if j~=i
            Z1{i} = Z1{i} + X{i,j}*(Y{i,j}*V{j});
          end
        end
      end
      Z1 = cell2mat(Z1);
      if lrth_type==1
        errloc(matind,lrthind) = mynorm(Z0-Z1)/(nrmA*nrmV);
      else
        errglob(matind,lrthind) = mynorm(Z0-Z1)/(nrmA*nrmV);
      end
    end
  end
end

h1=semilogy(errloc(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;
h2=semilogy(errloc(:,2),'-v','linewidth',lw,'markersize',ms);
h3=semilogy(errloc(:,3),'-s','linewidth',lw,'markersize',ms);
h1b=semilogy(errglob(:,1),'--o','linewidth',lw,'markersize',ms,'color',get(h1,'color'));
h2b=semilogy(errglob(:,2),'--v','linewidth',lw,'markersize',ms,'color',get(h2,'color'));
h3b=semilogy(errglob(:,3),'--s','linewidth',lw,'markersize',ms,'color',get(h3,'color'));
set(gca,'fontsize',fs);
hleg=legend('Local ($$\varepsilon=10^{-4}$$)','Local ($$\varepsilon=10^{-8}$$)','Local ($$\varepsilon=10^{-12}$$)','Global ($$\varepsilon=10^{-4}$$)','Global ($$\varepsilon=10^{-8}$$)','Global ($$\varepsilon=10^{-12}$$)');
set(hleg,'interpreter','latex','fontsize',fs,'numcolumns',2,'location','northoutside');
xlabel('$$n$$','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xticklabels(nlist);

ydata1 = get(h1b,'YData');
ydata2 = get(h2b,'YData');
ydata3 = get(h3b,'YData');
for i=1:length(nlist)
  if i==1
    xoffset=1.15;
  elseif i==length(nlist)
    xoffset=0.95;
  else
    xoffset=1;
  end
  htxt=text(i*xoffset,ydata1(i)/5,sprintf('%.0f',errglob(i,1)./errloc(i,1)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(i*xoffset,ydata2(i)/5,sprintf('%.0f',errglob(i,2)./errloc(i,2)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(i*xoffset,ydata3(i)/5,sprintf('%.0f',errglob(i,3)./errloc(i,3)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
end

function nrm = mynorm(A)
  nrm = norm(A,'fro');
end

function [X,Y] = compress(A,lrth,lrth_type,nrmA)
% compress based on truncated SVD
  [U,S,V] = svd(A);
  if lrth_type==1
    lrth_crit = mynorm(A);
  else
    lrth_crit = nrmA;
  end
  Svec = diag(S);
  rk = 1;
  while rk<length(Svec) && sqrt(sum(Svec(rk+1:end).^2))>lrth*lrth_crit
    rk = rk+1;
  end
  X = U(:,1:rk);
  Y = S(1:rk,1:rk)*V(:,1:rk)';
end
