% This function performs the BLR LU factorization of a dense unsymmetric
% matrix and returns the backward error and required flops. 
% =======
% Inputs:
% =======
%   matrix: name of the matrix file
%   blksiz: block size
%   lrth: low-rank threshold 
%   betatype: threshold type (beta parameter)
%     1 = local
%     2 = local scaled
%     3 = global
%     4 = global scaled
%   uprec: floating-point precision
%     'd' = double
%     's' = single
%     'h' = half
%   variant: BLR factorization algorithm
%     'UFC' = Update-Factor-Compress
%     'UCF' = Update-Compress-Factor
%     'FUC' = Factor-Update-Compress
%     'CUF' = Compress-Update-Factor
%   recomp: intermediate recompressions (0 = off, 1 = on)
%   output: output file where my_fprintf redirects  (if '', no redirection)
% ========
% Outputs:
% ========
%   err: backward error
%   flops: flop count for the factorization
%   Lsto, Usto: number of entries for the BLR L and U factors

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [err,flops,Lsto,Usto] = BLRstability(matrix,blksiz_in,lrth_in,betatype_in,uprec_in,variant_in,recomp_in,output)
%%%%%%%% init
  global fout;
  if strcmp(output,'')
    fout = 0;
  else
    fout = fopen(output,'a');
  end
  global lrth;
  lrth = lrth_in;
  global uprec;
  uprec = uprec_in;
  switch(uprec)
    case 'd', uprec_str = 'double';
    case 's', uprec_str = 'single';
    case 'h', uprec_str = 'half';
    otherwise, error('Wrong uprec parameter\n');
  end
  global betatype;
  betatype = betatype_in;
  switch(betatype)
    case 1, betatype_str = 'local';
    case 2, betatype_str = 'local, scaled';
    case 3, betatype_str = 'global';
    case 4, betatype_str = 'global, scaled';
    otherwise, error('Wrong betatype parameter\n');
  end
  global blksiz;
  blksiz = blksiz_in;
  global variant;
  variant = variant_in;
  global recomp;
  recomp = recomp_in;
  global flops;
  flops=0;
  [A,n,blknum,Afull] = read_matrix(matrix,blksiz);
  my_fprintf({'====================================\n'});
  my_fprintf({'Matrix name                 = %s\n',matrix});
  my_fprintf({'Matrix of order           n = %d\n',n});
  my_fprintf({'Number of block-rows/cols p = %d\n',blknum});
  my_fprintf({'Block size                b = %d\n',blksiz});
  my_fprintf({'Low-rank threshold          = %.0e\n',lrth});
  my_fprintf({'Threshold type (beta param) = %s\n',betatype_str});
  my_fprintf({'Floating-point precision    = %s\n',uprec_str});
  my_fprintf({'BLR factorization algorithm = %s\n',variant});
  my_fprintf({'Intermediate recompressions = %d\n',recomp});
  my_fprintf({'====================================\n'});
  if uprec=='d'
    Au = A;
  else
    Au = cell(blknum);
    for i=1:blknum
    for j=1:blknum
      Au{i,j} = single(A{i,j});
    end
    end
  end
%%%%%%%% facto
  LU = BLRLU(Au,blknum,Afull);
%%%%%%%% stats
  my_fprintf({'Flop count                  = %.1e (%.2f%% of classical LU)\n',flops,flops/2*3/n^3*100});
  [Lsto,Usto] = LvsU_compression(LU,blknum);
  err = bwderr(Afull,LU,blknum);
  my_fprintf({'BLR LU backward error       = %.2e\n',err});
  my_fprintf({'====================================\n'});
  if ~strcmp(output,'')
    fclose(fout);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function n = mynorm(A)
  n = norm(A,'fro');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function LU = BLRLU(A,blknum,Afull)
% computes BLR LU factors LU = A
% LU.X = row basis
% LU.Y = column basis
% LU.IsLR = low-rank info
  global variant;
  global betatype;
  beta = zeros(blknum);
  if betatype==1 || betatype==2
    % local threshold beta(i,j) = norm(A{i,j})
    for i=1:blknum
      for j=1:blknum
        beta(i,j) = mynorm(A{i,j});
      end
    end
  elseif betatype==3  || betatype==4
    % global threshold beta(i,j) = norm(A)
    normA = mynorm(Afull);
    for i=1:blknum
      for j=1:blknum
        beta(i,j) = normA;
      end
    end
  end

  % initialize LU with A
  LU = cell(blknum);
  for i=1:blknum
    for j=1:blknum
      LU{i,j}.X = A{i,j};
      LU{i,j}.IsLR = 0;
      if strcmp(variant,'CUF')==1
        if i>j
          [LU{i,j}] = compress(LU{i,j}.X,beta(i,j),'L');
        elseif i<j
          [LU{i,j}] = compress(LU{i,j}.X,beta(i,j),'U');
        end
      end
    end
  end

  %%%%%% MAIN LOOP %%%%%%
  for k=1:blknum
    %%%%%% COMPRESS step (UCF/CUF) %%%%%%
    if strcmp(variant,'UCF')==1
      for i=k+1:blknum
        [LU{i,k}] = compress(LU{i,k}.X,beta(i,k),'L');
        [LU{k,i}] = compress(LU{k,i}.X,beta(k,i),'U');
      end
    elseif strcmp(variant,'CUF')==1
      for i=k+1:blknum
        [LU{i,k}] = recompress(LU{i,k},beta(i,k),'L');
        [LU{k,i}] = recompress(LU{k,i},beta(k,i),'U');
      end
    end
    %%%%%% end COMPRESS step (UCF/CUF) %%%%%%

    %%%%%% FACTOR step %%%%%%
    LU{k,k}.X = mylu_nopiv(LU{k,k}.X); 
    for i=k+1:blknum
      [LU{i,k}] = solve(LU{k,k}.X,LU{i,k},'L');
      [LU{k,i}] = solve(LU{k,k}.X,LU{k,i},'U');
    end
    %%%%%% end FACTOR step %%%%%%

    %%%%%% COMPRESS step (UFC/FUC) %%%%%%
    if strcmp(variant,'UFC')==1 || strcmp(variant,'FUC')==1
      if betatype==2 || betatype==4
        scalU = mynorm(triu(LU{k,k}.X));
        scalL = mynorm(tril(LU{k,k}.X,-1)+eye(length(LU{k,k}.X)));
      else
        scalU = 1; scalL = 1;
      end
    end
    if strcmp(variant,'UFC')==1
      for i=k+1:blknum
        [LU{i,k}] = compress(LU{i,k}.X,beta(i,k)/scalU,'L');
        [LU{k,i}] = compress(LU{k,i}.X,beta(k,i)/scalL,'U');
      end
    end
    %%%%%% end COMPRESS step (UFC/FUC) %%%%%%
    
    %%%%%% UPDATE step %%%%%%
    for i=k+1:blknum
      for j=k+1:blknum
        if i>=j
          [LU{i,j}] = update(LU{i,k},LU{k,j},LU{i,j},-1,'L',beta(i,j));
        else
          [LU{i,j}] = update(LU{i,k},LU{k,j},LU{i,j},-1,'U',beta(i,j));
        end
      end
    end
    %%%%%% end UPDATE step %%%%%%

    %%%%%% COMPRESS step (FUC) %%%%%%
    if strcmp(variant,'FUC')==1
      for i=k+1:blknum
        [LU{i,k}] = compress(LU{i,k}.X,beta(i,k)/scalU,'L');
        [LU{k,i}] = compress(LU{k,i}.X,beta(k,i)/scalL,'U');
      end
    end
    %%%%%% end COMPRESS step (FUC) %%%%%%
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Z = solve(T,B,LorU)
% solves TZ = B for Z
  global flops;
  Z = B;
  L = tril(T,-1)+eye(length(T));
  U = triu(T);
  if B.IsLR && LorU=='L'
    Z.Y = B.Y/U; 
    f = prod(size(U))*size(B.Y,1);
  elseif LorU=='L'
    Z.X = B.X/U; 
    f = prod(size(U))*size(B.X,1);
  else
    Z.X = L\B.X; 
    f = length(L)*(length(L)-1)*size(B.X,2);
  end
  flops = flops + f;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function Z = update(A,B,C,sgn,LorU,betaM)
% computes Z = sgn*A*B + C
  global recomp;

  % first, compute P=A*B
  if A.IsLR && B.IsLR
    P.IsLR = 1;
    M = matmul(A.Y,B.X);
    if recomp
      [M] = compress(M,betaM,LorU,min(size(M))-1);
    else
      Mtemp.X = M;
      Mtemp.IsLR = 0;
      M = Mtemp;
    end
    if M.IsLR 
      P.X = matmul(A.X,M.X); P.Y = matmul(M.Y,B.Y);
    else
      if size(M,1) < size(M,2)
        P.X = A.X; P.Y = matmul(M.X,B.Y);
      else
        P.X = matmul(A.X,M.X); P.Y = B.Y;
      end
    end
  elseif A.IsLR
    P.IsLR = 1;
    P.X = A.X; P.Y = matmul(A.Y,B.X);
  elseif B.IsLR
    P.IsLR = 1;
    P.X = matmul(A.X,B.X); P.Y = B.Y;
  else
    P.IsLR = 0;
    P.X = matmul(A.X,B.X); 
  end

  % then, compute Z=sgn*P+C
  if C.IsLR && P.IsLR
    Z.X = [C.X,sgn*P.X]; Z.Y = [C.Y;P.Y];
    Z.IsLR = 1;
  elseif C.IsLR
    Z.X = matmul(C.X,C.Y)+sgn*P.X; 
    Z.IsLR = 0;
  elseif P.IsLR
    Z.X = C.X+sgn*matmul(P.X,P.Y); 
    Z.IsLR = 0;
  else
    Z.X = C.X+sgn*P.X; 
    Z.IsLR = 0;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function B = recompress(A,beta,LorU) 
  if ~A.IsLR || size(A.X,2)==0
    B = A;
    return
  end
  B.IsLR = 1;
  maxrk = size(A.X,1)*size(A.Y,2)/(size(A.X,1)+size(A.Y,2));
  if LorU=='U'
    C = compress(A.X,beta,'U',min(size(A.X)));
    B.X = C.X; B.Y = matmul(C.Y,A.Y);
%    C = compress(A.X,beta,'U',min(size(A.X)));
%    D = matmul(C.Y,A.Y);
%    E = compress(D,beta,'L',min(size(D)));
%    B.X = matmul(C.X,E.X); B.Y = E.Y;
  else
    C = compress(A.Y,beta,'L',min(size(A.Y)));
    B.X = matmul(A.X,C.X); B.Y = C.Y;
%    C = compress(A.Y,beta,'L',min(size(A.Y)));
%    D = matmul(A.X,C.X);
%    E = compress(D,beta,'L',min(size(D)));
%    B.X = E.X; B.Y = matmul(E.Y,C.Y);
  end
  if size(B.X,2) > maxrk
    B.IsLR = 0;
    B.X = matmul(B.X,B.Y);
    B.Y = [];
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function C = matmul(A,B)
  global flops;
  global uprec;
  if uprec=='h'
    C = matmul_half(A,B);
  else
    C = A*B;
  end
  flops = flops + 2*prod(size(A))*size(B,2);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function C = matmul_half(A,B)
  C = zeros(size(A,1),size(B,2));
  for k=1:size(A,2)
    C = chop(C + chop(A(:,k)*B(k,:), 11), 11);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function B = compress(A,beta,LorU,kmax)
% compress based on truncated SVD
  global lrth;
  global flops;
  if nargin<4
    s=size(A);
    kmax=prod(s)/sum(s);
  end
  Svec = svd(A);
  [U,Smat,V] = svd(A);
  rk = 1;
  n = length(Svec);
  while rk<min(n,kmax+1) && sqrt(sum(Svec(rk+1:end).^2))>lrth*beta
    rk = rk+1;
  end
  if (rk <= kmax)
    B.IsLR = 1;
    if LorU=='L'
      B.X = U(:,1:rk);
      B.Y = Smat(1:rk,1:rk)*V(:,1:rk)';
    else
      B.X = U(:,1:rk)*Smat(1:rk,1:rk);
      B.Y = V(:,1:rk)';
    end
  else
    B.IsLR = 0;
    B.X = A;
  end
  flops = flops + 2*prod(size(A))*min(rk,kmax);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function B = mylu_nopiv(A)
  % lu with no pivoting
  global flops;
  n = size(A, 1); 
  L = eye(n); 
  for k = 1 : n
    L(k + 1 : n, k) = A(k + 1 : n, k) / A(k, k);
    for l = k + 1 : n
      A(l, :) = A(l, :) - L(l, k) * A(k, :);
    end
  end
  U = A;
  B = tril(full(L),-1) + full(U);
  flops = flops + 2/3*size(A,1)^3;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function my_fprintf(args)
% printf to either file or terminal
  global fout;
  if fout == 0
    fprintf(args{1:end});
  else
    fprintf(fout,args{1:end});
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function err = bwderr(Afull,S,blknum)
  xtrue = ones(size(Afull,1),1);
  rhs = Afull*xtrue;
  y = fwd_subst(S,blknum,rhs);
  x = bwd_subst(S,blknum,y);
  normA = mynorm(Afull);
  normx = mynorm(xtrue);
  err = mynorm(Afull*x-rhs)/(normA*normx);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function y = fwd_subst(S,blknum,rhs)
  y=rhs;
  jend = 0;
  for j=1:blknum
    jbeg = jend+1;
    jend = jbeg + size(S{j,j}.X,1) -1;
    Ljj = tril(S{j,j}.X,-1)+eye(size(S{j,j}.X));
    y(jbeg:jend,:) = Ljj\y(jbeg:jend,:);
    iend = jend;
    for i=j+1:blknum
      ibeg = iend+1;
      iend = ibeg + size(S{i,j}.X,1) -1;
      if S{i,j}.IsLR
        y(ibeg:iend,:) = y(ibeg:iend,:) - S{i,j}.X*(S{i,j}.Y*y(jbeg:jend,:));
      else
        y(ibeg:iend,:) = y(ibeg:iend,:) - S{i,j}.X*y(jbeg:jend,:);
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function x = bwd_subst(S,blknum,y)
  x=y;
  ibeg = size(y,1)+1;
  for i=blknum:-1:1
    iend = ibeg-1;
    ibeg = iend - size(S{i,i}.X,1) +1;
    jend = iend;
    for j=i+1:blknum
      jbeg = jend+1;
      if S{i,j}.IsLR
        jend = jbeg + size(S{i,j}.Y,2) -1;
        x(ibeg:iend,:) = x(ibeg:iend,:) - S{i,j}.X*(S{i,j}.Y*x(jbeg:jend,:));
      else
        jend = jbeg + size(S{i,j}.X,2) -1;
        x(ibeg:iend,:) = x(ibeg:iend,:) - S{i,j}.X*x(jbeg:jend,:);
      end
    end
    Uii = triu(S{i,i}.X);
    x(ibeg:iend,:) = Uii\x(ibeg:iend,:);
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Lsto,Usto] = LvsU_compression(S,blknum)
  Lsto = 0; Usto = 0;
  for i=1:blknum
    for j=1:blknum
      if S{i,j}.IsLR
        sto=2*prod(size(S{i,j}.X));
      else
        sto=prod(size(S{i,j}.X));
      end
      if i>j
        Lsto = Lsto+sto;
      elseif i<j
        Usto = Usto+sto;
      else
        % i=j => diag blocks not counted either in L or U
      end
    end
  end
end

