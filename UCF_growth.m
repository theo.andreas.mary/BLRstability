clear all; close all;

ind=0;
nlist = [64:16:128];
for matrix={'P64','P80','P96','P112','P128'}
  ind = ind+1;
  fprintf('matrix = %s\n',matrix{1});
  [errUFC(ind,1),flopsUFC(ind,1)]=BLRstability(matrix{1},256,1e-04,4,'d','UFC',0,'tmp');
  [errUCF(ind,1),flopsUCF(ind,1)]=BLRstability(matrix{1},256,1e-04,4,'d','UCF',0,'tmp');
  [errUFC(ind,2),flopsUFC(ind,2)]=BLRstability(matrix{1},256,1e-08,4,'d','UFC',0,'tmp');
  [errUCF(ind,2),flopsUCF(ind,2)]=BLRstability(matrix{1},256,1e-08,4,'d','UCF',0,'tmp');
  [errUFC(ind,3),flopsUFC(ind,3)]=BLRstability(matrix{1},256,1e-12,4,'d','UFC',0,'tmp');
  [errUCF(ind,3),flopsUCF(ind,3)]=BLRstability(matrix{1},256,1e-12,4,'d','UCF',0,'tmp');
end

h1=semilogy(errUFC(:,1),'-o','linewidth',lw,'markersize',ms);
hold on;        
h2=semilogy(errUFC(:,2),'-v','linewidth',lw,'markersize',ms);
h3=semilogy(errUFC(:,3),'-s','linewidth',lw,'markersize',ms);
h1b=semilogy(errUCF(:,1),'--o','linewidth',lw,'markersize',ms,'color',get(h1,'color'));
h2b=semilogy(errUCF(:,2),'--v','linewidth',lw,'markersize',ms,'color',get(h2,'color'));
h3b=semilogy(errUCF(:,3),'--s','linewidth',lw,'markersize',ms,'color',get(h3,'color'));

set(gca,'fontsize',fs);
hleg=legend('UFC ($$\varepsilon=10^{-4}$$)','UCF ($$\varepsilon=10^{-8}$$)','UFC ($$\varepsilon=10^{-12}$$)','UCF ($$\varepsilon=10^{-4}$$)','UFC ($$\varepsilon=10^{-8}$$)','UCF ($$\varepsilon=10^{-12}$$)');
set(hleg,'interpreter','latex','fontsize',fs,'numcolumns',2,'location','northoutside');
xlabel('$$n$$','interpreter','latex','fontsize',fs);
ylabel('Backward error','interpreter','latex','fontsize',fs);
xlim([-inf inf]);
xticklabels(nlist.^2);
ylim([-inf 1e-3]);
yticks(10.^(-13:2:-3));

ydata1 = get(h1b,'YData');
ydata2 = get(h2b,'YData');
ydata3 = get(h3b,'YData');
for i=1:length(nlist)
  if i==1
    xoffset=0.15;
  elseif i==length(nlist)
    xoffset=-0.15;
  else
    xoffset=0;
  end
  htxt=text(i+xoffset,ydata1(i)*4,sprintf('%.0f',errUCF(i,1)./errUFC(i,1)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(i+xoffset,ydata2(i)*4,sprintf('%.0f',errUCF(i,2)./errUFC(i,2)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
  htxt=text(i+xoffset,ydata3(i)*4,sprintf('%.0f',errUCF(i,3)./errUFC(i,3)));
  set(htxt,'fontsize',fs,'interpreter','latex','HorizontalAlignment','center');
end

